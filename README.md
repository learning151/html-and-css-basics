# HTML and CSS basics

Repository for project used to learn basic things about HTML and CSS.

Steps:
1. Follow course step by step: https://www.youtube.com/playlist?list=PL4cUxeGkcC9ivBf_eKCPIAYXWzLlPAm6G
- each lesson should be folder named 'Lesson-<lesson_no>-<lesson_name>' e.g 'Lesson-1-Introduction'
- each catalog should have only files related with lesson
- each done lesson should be tagged

2. After done with course, watch this movie to repeat some fact and learn new one about web development: https://www.youtube.com/watch?v=qz0aGYrrlhU 

3. Try to fill up a test: https://www.w3schools.com/html/exercise.asp?filename=exercise_html_attributes1

Help:

1. Git
* `git add --all` - add all changes to commit
* `git commit -m "Comment for commit"` - add a commit with comment
* `git push origin master`- default push syntax, push to origin from a current branch (`master`)
* `git push -u origin master` - push commit with upstream, so git remember source and branch
* `git push` - after `git push -u origin master`
* `git tag -a 0.0` - create tag on current state of branch
* `git push origin 0.0` - push tag on remote